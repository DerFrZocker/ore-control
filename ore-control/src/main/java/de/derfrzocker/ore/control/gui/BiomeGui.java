package de.derfrzocker.ore.control.gui;

import de.derfrzocker.ore.control.OreControl;
import de.derfrzocker.ore.control.OreControlMessages;
import de.derfrzocker.ore.control.Permissions;
import de.derfrzocker.ore.control.api.Biome;
import de.derfrzocker.ore.control.api.WorldOreConfig;
import de.derfrzocker.ore.control.gui.copy.CopyAction;
import de.derfrzocker.ore.control.gui.copy.CopyBiomesAction;
import de.derfrzocker.ore.control.utils.OreControlUtil;
import de.derfrzocker.spigot.utils.Config;
import de.derfrzocker.spigot.utils.MessageUtil;
import de.derfrzocker.spigot.utils.MessageValue;
import de.derfrzocker.spigot.utils.gui.PageGui;
import de.derfrzocker.spigot.utils.gui.PageSettings;
import de.derfrzocker.spigot.utils.gui.VerifyGui;
import lombok.NonNull;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.permissions.Permissible;

import java.util.LinkedHashSet;
import java.util.Set;

public class BiomeGui extends PageGui<Biome> {

    @NonNull
    private final WorldOreConfig worldOreConfig;

    private final CopyAction copyAction;

    BiomeGui(final Permissible permissible, final WorldOreConfig worldOreConfig) {
        this.worldOreConfig = worldOreConfig;
        this.copyAction = null;

        final Set<Biome> biomes = new LinkedHashSet<>();

        for (Biome biome : Biome.values()) {
            if (!OreControl.is_1_14 && biome.isV1_14())
                continue;

            biomes.add(biome);
        }

        init(biomes.toArray(new Biome[0]), Biome[]::new, BiomeGuiSettings.getInstance(), this::getItemStack, this::handleNormalClick);

        addItem(BiomeGuiSettings.getInstance().getInfoSlot(), MessageUtil.replaceItemStack(BiomeGuiSettings.getInstance().getInfoItemStack(), getMessagesValues()));
        addItem(BiomeGuiSettings.getInstance().getBackSlot(), MessageUtil.replaceItemStack(BiomeGuiSettings.getInstance().getBackItemStack()), event -> openSync(event.getWhoClicked(), new WorldConfigGui(worldOreConfig, event.getWhoClicked()).getInventory()));

        if (Permissions.RESET_VALUES_PERMISSION.hasPermission(permissible))
            addItem(BiomeGuiSettings.getInstance().getResetValueSlot(), MessageUtil.replaceItemStack(BiomeGuiSettings.getInstance().getResetValueItemStack()), this::handleResetValues);

        if (Permissions.COPY_VALUES_PERMISSION.hasPermission(permissible))
            addItem(BiomeGuiSettings.getInstance().getCopyValueSlot(), MessageUtil.replaceItemStack(BiomeGuiSettings.getInstance().getCopyValueItemStack()), event -> openSync(event.getWhoClicked(), new WorldGui(new CopyBiomesAction(worldOreConfig, Biome.values())).getInventory()));
    }

    public BiomeGui(final WorldOreConfig worldOreConfig, final @NonNull CopyAction copyAction) {
        this.worldOreConfig = worldOreConfig;
        this.copyAction = copyAction;

        final Set<Biome> biomes = new LinkedHashSet<>();

        for (Biome biome : Biome.values()) {
            if (!OreControl.is_1_14 && biome.isV1_14())
                continue;

            if (copyAction.shouldSet(biome))
                biomes.add(biome);
        }
        init(biomes.toArray(new Biome[0]), Biome[]::new, BiomeGuiSettings.getInstance(), this::getItemStack, this::handleCopyAction);

        addItem(BiomeGuiSettings.getInstance().getInfoSlot(), MessageUtil.replaceItemStack(BiomeGuiSettings.getInstance().getInfoItemStack(), getMessagesValues()));
    }

    private ItemStack getItemStack(final Biome biome) {
        return MessageUtil.replaceItemStack(BiomeGuiSettings.getInstance().getBiomeItemStack(biome));
    }

    private void handleNormalClick(final Biome biome, final InventoryClickEvent event) {
        openSync(event.getWhoClicked(), new OreGui(worldOreConfig, biome, event.getWhoClicked()).getInventory());
    }

    private void handleCopyAction(final Biome biome, final InventoryClickEvent event) {
        copyAction.setBiomeTarget(biome);
        copyAction.next(event.getWhoClicked(), this);
    }

    private void handleResetValues(final InventoryClickEvent event) {
        if (OreControl.getInstance().getConfigValues().verifyResetAction()) {
            openSync(event.getWhoClicked(), new VerifyGui(clickEvent -> {
                for (Biome biome : Biome.values())
                    OreControlUtil.reset(this.worldOreConfig, biome);

                OreControl.getService().saveWorldOreConfig(worldOreConfig);
                openSync(event.getWhoClicked(), getInventory());
                OreControlMessages.RESET_VALUE_SUCCESS.sendMessage(event.getWhoClicked());
            }, clickEvent1 -> openSync(event.getWhoClicked(), getInventory())).getInventory());
            return;
        }

        for (Biome biome : Biome.values())
            OreControlUtil.reset(this.worldOreConfig, biome);

        OreControl.getService().saveWorldOreConfig(worldOreConfig);
        OreControlMessages.RESET_VALUE_SUCCESS.sendMessage(event.getWhoClicked());
    }

    private MessageValue[] getMessagesValues() {
        return new MessageValue[]{new MessageValue("world", worldOreConfig.getName())};
    }

    private static final class BiomeGuiSettings extends PageSettings {
        private static BiomeGuiSettings instance = null;

        private static BiomeGuiSettings getInstance() {
            if (instance == null)
                instance = new BiomeGuiSettings();

            return instance;
        }

        private BiomeGuiSettings() {
            super(OreControl.getInstance(), "data/biome_gui.yml");
            if (OreControl.is_1_14)
                getYaml().setDefaults(Config.getConfig(OreControl.getInstance(), "data/biome_gui_v1.14.yml"));

        }

        @Override
        public void reload() {
            super.reload();
            if (OreControl.is_1_14)
                getYaml().setDefaults(Config.getConfig(OreControl.getInstance(), "data/biome_gui_v1.14.yml"));
        }

        private ItemStack getBiomeItemStack(final Biome biome) {
            return getYaml().getItemStack("biomes." + biome.toString()).clone();
        }

        private ItemStack getInfoItemStack() {
            return getYaml().getItemStack("info.item_stack").clone();
        }

        private int getInfoSlot() {
            return getYaml().getInt("info.slot");
        }

        private ItemStack getBackItemStack() {
            return getYaml().getItemStack("back.item_stack").clone();
        }

        private int getBackSlot() {
            return getYaml().getInt("back.slot");
        }

        private int getResetValueSlot() {
            return getYaml().getInt("value.reset.slot");
        }

        private ItemStack getResetValueItemStack() {
            return getYaml().getItemStack("value.reset.item_stack").clone();
        }

        private int getCopyValueSlot() {
            return getYaml().getInt("value.copy.slot");
        }

        private ItemStack getCopyValueItemStack() {
            return getYaml().getItemStack("value.copy.item_stack").clone();
        }
    }

}
